import useModal from "../hooks/useModal";
import { x } from "@xstyled/emotion";
import { AnimatePresence, motion } from "framer-motion";
import useUser from "../hooks/useUser";
import { TechnicalTrainingResponse } from "../types/content";
import { Button, Icon } from "anolis-ui";
import Icons from "./icons/Icons";
import { useEffect, useState } from "react";
import axios, { Axios, AxiosError } from "axios";

const Modal: React.FC<{
  data: TechnicalTrainingResponse;
}> = ({ data }) => {
  const { modalEventId, show, setShow } = useModal();
  const [error, setError] = useState("");
  const { email, registerEvent, locale } = useUser();

  useEffect(() => {
    setFormData({ ...formData, email });
  }, [email]);

  const [formData, setFormData] = useState({
    email,
  });

  return (
    <x.div
      pointerEvents={!show ? "none" : "all"}
      as={motion.div}
      position={"fixed"}
      zIndex={90}
      left={0}
      top={0}
      w={"100%"}
      // @ts-ignore
      initial={{
        backgroundColor: "transparent",
      }}
      animate={{
        backgroundColor: show ? "#000000a9" : "transparent",
      }}
      h={"100%"}
      onClick={() => {
        setShow(false);
      }}
    >
      <x.div
        position={"absolute"}
        left="50%"
        top={"50%"}
        onClick={(e) => {
          e.stopPropagation();
        }}
        transform={"translate(-50%, -50%)"}
        pointerEvents="all"
        as={motion.div}
        backgroundColor="#f0e9ff"
        padding={"2rem"}
        borderRadius="5px"
        display={"flex"}
        flexDirection={{ _: "column", md: "row" }}
        w={{ _: "90vw", lg: "70vw" }}
        h={{ _: "80vh", lg: "70vh" }}
        //@ts-ignore
        initial={{
          scale: 0,
          opacity: 0,
        }}
        animate={show ? "show" : "hide"}
        variants={{
          show: {
            opacity: 1,
            scale: 1,
            x: "-50%",
            y: "-50%",
          },
          hide: {
            opacity: 0,
            scale: 0,
            x: "-50%",
            y: "-50%",
          },
        }}
      >
        {show && (
          <>
            <x.div flex={1}>
              <x.h2 fontWeight="bold" fontSize="2rem">
                {
                  data.content.values.eventsList[modalEventId as any]
                    .trainingType[locale]
                }
              </x.h2>
              <x.div
                marginTop="1rem"
                marginBottom="1rem"
                display={"flex"}
                alignItems="center"
              >
                <x.span marginRight="2rem">
                  <Icon
                    marginRight={".5rem"}
                    svg={<Icons.Calendar></Icons.Calendar>}
                  ></Icon>{" "}
                  {data.content.values.eventsList[modalEventId as any].date}
                </x.span>
                <x.span marginRight="2rem">
                  <Icon
                    marginRight={".5rem"}
                    svg={<Icons.Position></Icons.Position>}
                  ></Icon>
                  {
                    data.content.values.eventsList[modalEventId as any]
                      .location[locale]
                  }
                </x.span>
              </x.div>
              <x.p color="gray" marginBottom=".5rem">
                {data.content.values.modalDescLabel[locale]}
              </x.p>
              {data.content.values.mailDesc[locale]}
            </x.div>
            <x.div flex="1">
              <x.p color="gray" marginBottom=".5rem">
                {data.content.values.modalMailToLabel[locale]}
              </x.p>
              <x.input
                padding="1rem"
                type="text"
                value={formData.email}
                onChange={(e) => {
                  setFormData({ ...formData, email: e.target.value });
                }}
              />
              <Button
                onClick={() => {
                  const _ = async () => {
                    try {
                      const result = await axios.post("/api/submit", {
                        eventId: parseInt(
                          data.content.values.eventsList[modalEventId as any].id
                        ),
                        email: formData.email,
                      });
                      if (result.status === 200) {
                        registerEvent(modalEventId.toString());
                        setShow(false);
                      }
                    } catch (err) {
                      setError(`Error: ${err}`);
                    }
                  };
                  _();
                }}
              >
                {data.content.values.modalSendBtn[locale]}
              </Button>
              <div className="error">{error}</div>
            </x.div>
          </>
        )}
      </x.div>
    </x.div>
  );
};

export default Modal;
