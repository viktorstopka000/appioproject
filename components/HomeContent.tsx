import { x } from "@xstyled/emotion";
import Head from "next/head";
import { useEffect, useState } from "react";

import useSWR from "swr";
import { fetcher } from "../pages";
import useUser from "../hooks/useUser";
import { TechnicalTrainingResponse } from "../types/content";
import ListedEvent from "./ListedEvent";
import Modal from "./Modal";
import useBreakpoint, { Breakpoint } from "../hooks/useBreakpoint";
import MobileListedEvent from "./MobileListedEvent";
import Dropdown, { DropdownItem } from "./Dropdown";

const HomeContent: React.FC = () => {
  const { locale } = useUser();
  const b = useBreakpoint();
  const { data } = useSWR<TechnicalTrainingResponse>(
    `https://dev.robe.wnh.cz/api/content/technicalTraining?published=true&locales=${locale}`,
    fetcher
  );
  const [filtered, setFiltered] = useState(data);
  const [filterBy, setFilterBy] = useState(0);
  useEffect(() => {
    switch (filterBy) {
      case 0:
        setFiltered(data);
        break;
      case 1:
        if (data) {
          setFiltered({
            ...data,
            content: {
              ...data.content,
              values: {
                ...data.content.values,
                eventsList: data.content.values.eventsList.filter((e) => {
                  const now = new Date();
                  const eventDate = new Date(e.date);
                  if (eventDate < now) {
                    return false;
                  }
                  return true;
                }),
              },
            },
          });
        }
        break;
      case 2:
        if (data) {
          setFiltered({
            ...data,
            content: {
              ...data.content,
              values: {
                ...data.content.values,
                eventsList: data.content.values.eventsList.filter((e) => {
                  if (!e.active) return false;
                  const now = new Date();
                  const eventDate = new Date(e.date);
                  if (eventDate < now) {
                    return false;
                  }
                  return true;
                }),
              },
            },
          });
        }
        break;
      default:
        console.error("Unhandled dropdown option.");
        break;
    }
  }, [filterBy, data]);

  return (
    <>
      <Head>
        <title>{data?.content.values.title[locale]}</title>
      </Head>

      <x.h1
        fontSize="3rem"
        lineHeight="1"
        fontWeight="bold"
        textAlign={"center"}
        marginTop="4rem"
        marginBottom="4rem"
      >
        {data?.content.values.title[locale]}
      </x.h1>

      <x.p>{data?.content.values.intro[locale]}</x.p>
      <x.div className="dropdown" marginTop={"2rem"} marginBottom={"1rem"}>
        <Dropdown name="Show" dispatch={setFilterBy}>
          <DropdownItem name="Show all" action={() => {}}></DropdownItem>
          <DropdownItem
            name="Hide past events"
            action={() => {}}
          ></DropdownItem>
          <DropdownItem name="Only available" action={() => {}}></DropdownItem>
        </Dropdown>
      </x.div>
      {b > Breakpoint.Mobile ? (
        <x.table width={"100%"} marginTop={"1rem"} borderCollapse={"collapse"}>
          {filtered?.content.values.eventsList?.map((event, i) => (
            <ListedEvent key={event.id} {...event} orderId={i}></ListedEvent>
          ))}
        </x.table>
      ) : (
        <x.div marginTop="1rem">
          {filtered?.content.values.eventsList?.map((event, i) => (
            <MobileListedEvent
              key={event.id}
              {...event}
              orderId={i}
            ></MobileListedEvent>
          ))}
        </x.div>
      )}
      <Modal data={filtered as TechnicalTrainingResponse}></Modal>
    </>
  );
};

export default HomeContent;
