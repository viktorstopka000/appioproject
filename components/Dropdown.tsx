import React, { useEffect, useMemo, useState } from "react";
import { x } from "@xstyled/emotion";
import { style } from "../pages/_app";
import { AnimatePresence, motion } from "framer-motion";
import { Icon } from "anolis-ui";
import Icons from "./icons/Icons";
import Math2 from "../utils/Math2";

const Dropdown: React.FC<{
  name: string;
  children: JSX.Element[];
  dispatch: (id: number) => void;
}> = ({ name, children, dispatch }) => {
  const [active, setActive] = useState(0);
  const [open, setOpen] = useState(false);
  useEffect(() => {
    children[active].props.action && children[active].props.action();
    dispatch(active);
  }, [active]);
  const height = useMemo(() => {
    return Math2.Clamp(children.length, 0, 5) * 2.5;
  }, []);
  return (
    <x.div display={"flex"} alignItems="center">
      <x.div
        onClick={() => {
          setOpen(false);
        }}
        w={"100%"}
        h={"100%"}
        left="0"
        top="0"
        position={"fixed"}
        pointerEvents={open ? "all" : "none"}
      ></x.div>
      {name}:{" "}
      <x.div
        marginLeft="1rem"
        as={motion.div}
        fontSize=".9rem"
        w={"20rem"}
        h={"2.5rem"}
        backgroundColor={style.grey}
        borderRadius="10px"
        cursor={"pointer"}
        display="flex"
        justifyContent={"center"}
        alignItems={"center"}
        position="relative"
        // @ts-ignore
        whileHover={{ scale: 1.05 }}
        onClick={() => {
          setOpen(!open);
        }}
      >
        {children[active].props.name}
        <Icon marginLeft="1rem">
          <Icons.Down></Icons.Down>
        </Icon>
        <AnimatePresence>
          {open && (
            <x.div
              as={motion.div}
              key="dropdownOpen"
              //   @ts-ignore
              initial={{
                scale: 0,
                y: `${(height / 2.5) * 10 * 2}%`,
              }}
              animate={{
                scale: 1,
                y: `${(height / 2.5) * 10 * 2}%`,
              }}
              exit={{
                scale: 0,

                y: `${(height / 2.5) * 10 * 2}%`,
              }}
              position={"absolute"}
              w={"100%"}
              h={`${height}rem`}
              backgroundColor={style.grey}
              borderRadius="10px"
            >
              {children.map((item, i) => {
                return (
                  <x.div
                    key={`o${i}`}
                    h={"2.5rem"}
                    as={motion.div}
                    // @ts-ignore
                    whileHover={{
                      backgroundColor: style.lred,
                      transition: {
                        duration: 0.2,
                      },
                    }}
                    display="flex"
                    justifyContent={"center"}
                    alignItems="center"
                    onClick={() => {
                      setActive(i);
                    }}
                  >
                    {item.props.name}
                  </x.div>
                );
              })}
            </x.div>
          )}
        </AnimatePresence>
      </x.div>
    </x.div>
  );
};

export const DropdownItem: React.FC<{
  action?: () => void;
  name: string;
}> = ({ action, name }) => {
  return <></>;
};

export default Dropdown;
