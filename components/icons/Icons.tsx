const Position: React.FC = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="21"
      height="30"
      viewBox="0 0 21 30"
    >
      <path
        d="M18 3A10.492 10.492 0 007.5 13.5C7.5 21.375 18 33 18 33s10.5-11.625 10.5-19.5A10.492 10.492 0 0018 3zm0 14.25a3.75 3.75 0 113.75-3.75A3.751 3.751 0 0118 17.25z"
        data-name="Icon material-location-on"
        transform="translate(-7.5 -3)"
      ></path>
    </svg>
  );
};

const Calendar: React.FC = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="21"
      height="24"
      viewBox="0 0 21 24"
    >
      <path
        d="M.563 9h19.875a.564.564 0 01.562.563V21.75A2.251 2.251 0 0118.75 24H2.25A2.251 2.251 0 010 21.75V9.563A.564.564 0 01.563 9zM21 6.938V5.25A2.251 2.251 0 0018.75 3H16.5V.563A.564.564 0 0015.938 0h-1.875a.564.564 0 00-.563.563V3h-6V.563A.564.564 0 006.938 0H5.063A.564.564 0 004.5.563V3H2.25A2.251 2.251 0 000 5.25v1.688a.564.564 0 00.563.562h19.875A.564.564 0 0021 6.938z"
        data-name="Icon awesome-calendar"
      ></path>
    </svg>
  );
};

const Down: React.FC = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="22.493"
      height="14.527"
      viewBox="0 0 22.493 14.527"
      className="icon-down"
    >
      <path
        d="M10.055 24.771L.492 15.209a1.681 1.681 0 010-2.384l1.589-1.589a1.681 1.681 0 012.384 0l6.778 6.778 6.778-6.778a1.681 1.681 0 012.384 0l1.589 1.589a1.681 1.681 0 010 2.384l-9.562 9.562a1.672 1.672 0 01-2.377 0z"
        data-name="Icon awesome-angle-down"
        transform="translate(.004 -10.74)"
      ></path>
    </svg>
  );
};

const Icons = { Position, Calendar, Down };

export default Icons;
