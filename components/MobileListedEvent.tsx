import { x } from "@xstyled/emotion";
import { useEffect, useMemo } from "react";
import useSWR from "swr";
import { Button, Icon } from "anolis-ui";
import { fetcher } from "../pages";
import {
  TechnicalTrainingEvent,
  TechnicalTrainingResponse,
} from "../types/content";
import useModal from "../hooks/useModal";
import Icons from "./icons/Icons";
import useUser from "../hooks/useUser";
import { style } from "../pages/_app";

enum EventState {
  ACTIVE,
  FULL,
  PAST,
  NODATE,
}

const MobileListedEvent: React.FC<
  TechnicalTrainingEvent & { orderId: number }
> = (props) => {
  const { setModalEventId, setShow } = useModal();
  const { registeredEvents, locale } = useUser();
  const success = useMemo(() => {
    return registeredEvents.includes(props.orderId.toString());
  }, [registeredEvents]);
  useEffect(() => {
    console.log(registeredEvents);
  }, [registeredEvents]);
  const { data } = useSWR<TechnicalTrainingResponse>(
    `https://dev.robe.wnh.cz/api/content/technicalTraining?published=true&locales=${locale}`,
    fetcher
  );
  const eventState = useMemo(() => {
    if (props.date) {
      const now = new Date();
      const eventDate = new Date(props.date);
      if (eventDate < now) {
        return EventState.PAST;
      }
      if (!props.active) {
        return EventState.FULL;
      }
      return EventState.ACTIVE;
    }
    return EventState.NODATE;
  }, [props.date, props.active]);
  return (
    <x.div
      paddingTop="1rem"
      paddingBottom="1rem"
      borderBottom={`2px solid ${style.grey}`}
    >
      <x.div
        marginTop="1rem"
        marginBottom="1rem"
        fontWeight="bold"
        textAlign={"center"}
      >
        {props.trainingType[locale]}
      </x.div>
      <x.div
        display={"flex"}
        justifyContent="space-around"
        marginTop="1rem"
        marginBottom="1rem"
      >
        <x.div display={"flex"}>
          <Icon
            marginRight={".5rem"}
            fill={style.red}
            svg={Icons.Position}
          ></Icon>{" "}
          {props.location[locale]}
        </x.div>
        <x.div display={"flex"}>
          <Icon
            marginRight={".5rem"}
            fill={style.red}
            svg={Icons.Calendar}
          ></Icon>{" "}
          {props.date}
        </x.div>
      </x.div>
      {eventState !== EventState.PAST && (
        <Button
          w={"100%"}
          backgroundColor={success ? "#ed7874" : "#e23737"}
          onClick={(e) => {
            e.preventDefault();
            setModalEventId(props.orderId.toString());
            setShow(true);
          }}
          disabled={
            eventState === EventState.FULL || eventState === EventState.NODATE
          }
        >
          {eventState === EventState.ACTIVE
            ? !success
              ? data?.content.values.inquiryBtn[locale]
              : "Successfully booked"
            : data?.content.values.InquiryBtnDeactivated[locale]}
        </Button>
      )}
    </x.div>
  );
};

export default MobileListedEvent;
