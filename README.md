# Technical training calendar

We'd like to have the ability for subsidiaries to create appointment calendars for technical trainings under Spot On. Basically it will be an adoption of the "expos&events" calendar.
in the backend, there should be title, intro, and then content blocks similar to "contact / offices / details" with subheading, rich text field, smart links and image. 

The event list should have the columns date, type of training, location, and a button "inquire". Clicking on the button should open up a detail with signup form with an email prewritten. The inquire button should disappear for past events, and be manually unchecked in admin and then appear greyed out as "fully booked".

## Production website
https://www.robe.cz/

## Backend endpoints

**List training events (with all field values)**

**Get training event detail**
```
https://dev.robe.wnh.cz/api/content/technicalTraining?published=true&locales=en
```

## Used libraries

- Next.js
- SWR
- emotion.js
- xstyled
- anolis ui

## More resources

https://doc.clickup.com/4687466/p/h/4f1ka-1261/d5af844099525cf

https://anolis-ui.com/
