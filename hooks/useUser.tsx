import create from "zustand";

type UserState = {
  email: string;
  setEmail: (email: string) => void;

  registeredEvents: string[];
  registerEvent: (id: string) => void;

  locale: string;
  setLocale: (locale: string) => void;
};

const useUser = create<UserState>((set) => ({
  email: "",
  setEmail: (email) => {
    set((state) => ({ ...state, email }));
  },
  registeredEvents: [],
  registerEvent: (id) => {
    console.log(id);
    set((state) => ({
      ...state,
      registeredEvents: [...state.registeredEvents, id],
    }));
  },
  locale: "en",
  setLocale: (locale) => {
    set((state) => ({ ...state, locale }));
  },
}));

export default useUser;
