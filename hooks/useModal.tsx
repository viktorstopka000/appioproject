import create from "zustand";

type ModalState = {
  modalEventId: string;
  setModalEventId: (modalEventId: string) => void;

  show: boolean;
  setShow: (show: boolean) => void;
};

const useModal = create<ModalState>((set) => ({
  modalEventId: "1",
  setModalEventId: (modalEventId) => {
    set((state) => ({
      ...state,
      modalEventId,
    }));
  },
  show: false,
  setShow: (show) => {
    set((state) => ({
      ...state,
      show,
    }));
  },
}));

export default useModal;
