export type TechnicalTrainingResponse = ContentResponse<TechnicalTraining>;

export interface ContentResponse<T extends Content = Content> {
  locales: { id: string; name: string }[];

  categoryTypes: any[];
  content: T;
}

export type TechnicalTrainingEvent = {
  id: string;
  date: string;
  active: boolean;
  location: Localized<string>;
  trainingType: Localized<string>;
};

// eslint-disable-next-line max-len
export type TechnicalTraining<
  TKey extends keyof TechnicalTrainingValues = keyof TechnicalTrainingValues
> = Content<TechnicalTrainingValues, undefined, "technicalTraining", TKey>;
export interface TechnicalTrainingValues {
  intro: Localized<string>;
  title: Localized<string>;
  mailDesc: Localized<string>;
  eventsList: Array<TechnicalTrainingEvent>;
  inquiryBtn: Localized<string>;
  introBlocks:
    | Array<{
        text: Localized<string> | undefined;
        image: File | undefined;
        subtitle: Localized<string> | undefined;
      }>
    | undefined;
  mailSubject: Localized<string>;
  modalSendBtn: Localized<string>;
  modalDescLabel: Localized<string>;
  modalMailToLabel: Localized<string>;
  modalSubjectLabel: Localized<string>;
  InquiryBtnDeactivated: Localized<string>;
}
export interface ContentBase<TType extends string = string> {
  id: number;
  type: TType;
  title: string | Record<string, string>;
  slug?: string | Record<string, string>;
  enabled?: boolean | Record<string, boolean>;
}

export interface Content<
  TValue extends {} = {},
  TCategories extends {} | undefined = {} | undefined,
  TType extends string = string,
  TKey extends keyof TValue = keyof TValue
> extends ContentBase<TType> {
  locales?: { name: string }[];
  createdAt: string;
  updatedAt: string;
  publishedAt: string | Localized<string>;

  values: Pick<TValue, TKey>;

  categories?: TCategories;
  contentOf: number[];
}

export type Localized<T = string> = Record<string, T>;
