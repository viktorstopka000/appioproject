import { Preflight, ThemeProvider, defaultTheme } from "@xstyled/emotion";
import type { AppProps } from "next/app";
import { AnolisProvider, buttonTheme, createTheme } from "anolis-ui";
import { FC } from "react";
import "../style/globals.css";
import Head from "next/head";

const MyApp: FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <ThemeProvider theme={defaultTheme}>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <AnolisProvider theme={theme}>
        <Preflight />
        <Component {...pageProps} />
      </AnolisProvider>
    </ThemeProvider>
  );
};

export default MyApp;

const theme = createTheme({
  ...buttonTheme({
    variants: {
      solid: {
        bg: "rgb(224 0 52)",
      },
    },
  }),
});

export const style = {
  red: "rgb(224 0 52)",
  lred: "rgb(255, 85, 124)",
  grey: "#e6e6e6",
};
