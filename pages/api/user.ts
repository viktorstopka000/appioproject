// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { faker } from "@faker-js/faker";

export type Data = {
  email: string;
};

const email = faker.internet.email();

export default (req: NextApiRequest, res: NextApiResponse<Data>) => {
  if (req.cookies["token"] !== "1234token56789") {
    res.status(401).end("Unauthorized");
    return;
  }

  res.status(200).json({ email });
};
