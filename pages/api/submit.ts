import type { NextApiRequest, NextApiResponse } from "next";

export default (req: NextApiRequest, res: NextApiResponse) => {
  if (req.cookies["token"] !== "1234token56789") {
    res.status(401).end("Unauthorized");
    return;
  }

  if (
    typeof req.body.eventId !== "number" ||
    typeof req.body.email !== "string"
  ) {
    res.status(400).end({ error: true });
    return;
  }

  res.status(200).json({ success: true });
};
