import { x } from "@xstyled/emotion";
import { Container } from "anolis-ui";
import type { GetStaticProps, NextPage } from "next";
import { SWRConfig } from "swr";
import axios from "axios";
import { TechnicalTrainingResponse } from "../types/content";
import HomeContent from "../components/HomeContent";
import { useEffect } from "react";
import useUser from "../hooks/useUser";

export const fetcher = async (url: string) => {
  console.log("FetchSWR");
  return axios.get(url).then((res) => res.data);
};

interface Props {
  fallback: TechnicalTrainingResponse;
}

//& Static fetch
export const getStaticProps: GetStaticProps<Props> = async () => {
  const res = await axios.get(
    "https://dev.robe.wnh.cz/api/content/technicalTraining?published=true&locales=en"
  );
  return {
    props: {
      fallback: res.data,
    },
  };
};

const Home: NextPage<Props> = ({ fallback }) => {
  const { email, setEmail } = useUser();
  useEffect(() => {
    const _ = async () => {
      const login = await axios.post("/api/login", null, {
        withCredentials: true,
      });
      if (login.data.success) {
        const user = await axios.get("/api/user");
        if (user.status === 200) {
          setEmail(user.data.email);
        }
      }
    };
    _();
  }, []);
  return (
    <SWRConfig value={{ fallback }}>
      <Container>
        <HomeContent></HomeContent>
      </Container>
    </SWRConfig>
  );
};

export default Home;
